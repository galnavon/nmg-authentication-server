FROM node:10.14.2

LABEL authors=CHP_TEAM

USER root

# arguments and environment variables
ARG ssh_prv_key

#============================================
# apt installs
#============================================
RUN apt update
RUN apt install -y openssh-client git gcc build-essential freetds-dev unzip


#=================================
# Authorize SSH Host && Add the keys and set permissions
#=================================
RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    ssh-keyscan bitbucket.org > /root/.ssh/known_hosts && \
    echo "$ssh_prv_key" > /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa


#=================================
# git clone and npm i
#=================================
RUN cd /usr/local && git clone git@bitbucket.org:namogooteam/nmg-authentication-server.git && cd nmg-authentication-server && npm i

# just for local env
COPY ./config.json /root/.nmg/config.json

WORKDIR /usr/local/nmg-authentication-server

#=================================
# Run
#=================================
CMD ["node", "server/bin/www"]
