const express = require('express');
const getSSOHandlers = require('./sso.ctrl/ssoHandlers');

/**
 * Error Handler Wrapper
 * @param fn
 */

const router = express.Router();

(async function buildSSOHandlers() {
  const ssoHandlers = await getSSOHandlers(router);

  // generate sso handlers
  ssoHandlers.forEach((client) => {
    router.post(`/${client.domain}`, client.handler, (req, res) => {
      res.redirect('/');
    });
  });
}());

// handle not configured sso domains
router.post('/:domain', (req, res) => res.status(401).send('Unauthorized'));

module.exports = router; // exporting router so that it can be used in app.js
