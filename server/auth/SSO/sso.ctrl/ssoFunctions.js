const fs = require('fs'); // fs
const path = require('path'); // path
const logger = require('nmg-utils').logger({ consoleOn: true, logzioOn: true });
const createDebug = require('debug'); // debugging utility

const debug = createDebug('nmg-authentication-server:app'); // create the debug object
const authenticationCtrl = require('../../auth.ctrl/authentication.ctrl');

const successLoginRedirect = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'dev' ? '/' : '//localhost:8080/';
const failureLoginRedirect = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'dev' ? '/login' : '//localhost:8080/';

const getSSOClientsConfiguration = async () => {
  return [
    {
      domain: 'gap.com',
      entryPoint: 'https://onelogonqa.gap.com/idp/SSO.saml2',
      issuer: 'NAMOGOO',
      cert: 'MIIDYjCCAkqgAwIBAgIGAVUxcjywMA0GCSqGSIb3DQEBCwUAMHIxCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNU2FuIEZyYW5jaXNjbzENMAsGA1UEChMERUFNUzEPMA0GA1UECxMGR2FwIElUMR4wHAYDVQQDExVTQU1MMlNpZ25lclFBLmdhcC5jb20wHhcNMTYwNjA4MTkxNjU4WhcNMjYwNjA2MTkxNjU4WjByMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDVNhbiBGcmFuY2lzY28xDTALBgNVBAoTBEVBTVMxDzANBgNVBAsTBkdhcCBJVDEeMBwGA1UEAxMVU0FNTDJTaWduZXJRQS5nYXAuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAswnXZkHrKrDqKi4OW2gECcnkEWRQgMSpvdLhqGCTBxRn2bKsTnhyB1AsL2QN27YI3c9hV3du9RfjgxomyxdPSTHsJuIoHa2+l2H77oVFl1pPQn2K6AhubcoM+zAuWR+K5zOqzD8J8feOM89uznWd3edCDdoRyoWNCZh5ymV3W6G6s2P6iWMg/KqKLwMKsW4PnZu5QXge+c0GIy50ZSfEW6a+b7daHS52zlHLMENkUdaEma7aFp+I345fSz61ty2qjtgALGbfX9ZecFVjj/yRNcbyOA6x9hNFHMk9F/nS0tNkNmrimjC9X+/gYuvP/ubIrZBeeSU26YcHhBt8dZ6RuwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQATm5Dk8GMQR0cqMBx4LdtdhEJXK22W+1h0MFzJTGj0Ke3ufPaoiguvfmxrRfNoXS1eJGRXpTg4Yuei9Qhq/z2u2GhonKvLr+uvCJzPr8SWb63NQ0deIwUufggeE768u+xKYyTFIpJG+qcDqjAwkvi01gugal2qDu9z9YEba2Kf5C19Mus+DrX+yQ40Jltl1ljRM/OsfXy0WlSbXeIMLNE5FSw9FZ0iy+J2Chl9r9lFBib0EtrdAxPBIxsSym7A91zQu7WXCBMwOxEsJGyOyiVMeHoku93MC1G637qZ825c+RJTKEFrHKQgCcPmlmiVZGP0IFMCZFbeQlzadhpBTF3H',
      authnRequestBinding: 'HTTP-POST',
      skipRequestCompression: true,
    },
    {
      domain: 'wayfair.com',
      entryPoint: 'https://wayfair.okta.com/home/wayfairproduction_namogoo_1/0oa8ad28ztMZXeTmW1t7/aln8ad46f98OQ2Ys81t7',
      issuer: 'http://www.okta.com/exk8ad28zsipsNDKE1t7',
      cert: 'MIIDYjCCAkqgAwIBAgIGAVUxcjywMA0GCSqGSIb3DQEBCwUAMHIxCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNU2FuIEZyYW5jaXNjbzENMAsGA1UEChMERUFNUzEPMA0GA1UECxMGR2FwIElUMR4wHAYDVQQDExVTQU1MMlNpZ25lclFBLmdhcC5jb20wHhcNMTYwNjA4MTkxNjU4WhcNMjYwNjA2MTkxNjU4WjByMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDVNhbiBGcmFuY2lzY28xDTALBgNVBAoTBEVBTVMxDzANBgNVBAsTBkdhcCBJVDEeMBwGA1UEAxMVU0FNTDJTaWduZXJRQS5nYXAuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAswnXZkHrKrDqKi4OW2gECcnkEWRQgMSpvdLhqGCTBxRn2bKsTnhyB1AsL2QN27YI3c9hV3du9RfjgxomyxdPSTHsJuIoHa2+l2H77oVFl1pPQn2K6AhubcoM+zAuWR+K5zOqzD8J8feOM89uznWd3edCDdoRyoWNCZh5ymV3W6G6s2P6iWMg/KqKLwMKsW4PnZu5QXge+c0GIy50ZSfEW6a+b7daHS52zlHLMENkUdaEma7aFp+I345fSz61ty2qjtgALGbfX9ZecFVjj/yRNcbyOA6x9hNFHMk9F/nS0tNkNmrimjC9X+/gYuvP/ubIrZBeeSU26YcHhBt8dZ6RuwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQATm5Dk8GMQR0cqMBx4LdtdhEJXK22W+1h0MFzJTGj0Ke3ufPaoiguvfmxrRfNoXS1eJGRXpTg4Yuei9Qhq/z2u2GhonKvLr+uvCJzPr8SWb63NQ0deIwUufggeE768u+xKYyTFIpJG+qcDqjAwkvi01gugal2qDu9z9YEba2Kf5C19Mus+DrX+yQ40Jltl1ljRM/OsfXy0WlSbXeIMLNE5FSw9FZ0iy+J2Chl9r9lFBib0EtrdAxPBIxsSym7A91zQu7WXCBMwOxEsJGyOyiVMeHoku93MC1G637qZ825c+RJTKEFrHKQgCcPmlmiVZGP0IFMCZFbeQlzadhpBTF3H',
      authnRequestBinding: null,
      skipRequestCompression: null,
    },
    {
      domain: 'dsc.com',
      entryPoint: 'https://dollarshaveclub.okta.com/app/dollarshaveclub_namogoo_1/exkksn2yrvUb9qP5A0x7/sso/saml',
      issuer: 'http://www.okta.com/exkksn2yrvUb9qP5A0x7',
      cert: 'MIIDrjCCApagAwIBAgIGAV2mjk7AMA0GCSqGSIb3DQEBCwUAMIGXMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxGDAWBgNVBAMMD2RvbGxhcnNoYXZlY2x1YjEcMBoGCSqGSIb3DQEJARYNaW5mb0Bva3RhLmNvbTAeFw0xNzA4MDMwNTI0MzBaFw0yNzA4MDMwNTI1MjlaMIGXMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxGDAWBgNVBAMMD2RvbGxhcnNoYXZlY2x1YjEcMBoGCSqGSIb3DQEJARYNaW5mb0Bva3RhLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJRKEEUvj+OEd2QztqiBmMK2OaA59Ifp3pbAj2/Fdw4YWbahf4X+YWEyRlGxmH0e3C17kLhLbmt7xCgTSb3CU5jXV0mFN3tcvYtpj+w8WqCHI/RksMJJvdZPwU4VVGgwAT540LXaXI40auFt36YWJcoPD1aE7TtOvLQNAeKlGjtdxilthGkF38QOfZDgwuQUVqjtXLvPb8JbJtVVHHAjaVABxOOMp4pbgqeut47WOu965qkNari72kiapPwVirCeNhmTCRJxa5eGK7s61TgjCaoRxVhs+rb2BIJlcSDQ3F5yxva5mT1086uvavOe0T/4ko9zlg5dLoNVrjaJPG2XzEUCAwEAATANBgkqhkiG9w0BAQsFAAOCAQEATH/Mhr1SweviA/KT1IbfAJEtkR9Y/hJIpotR2tmm03QI9amS8siffEmSAy9wy4y/hc/RVFjGzENP8BptuYCbOwf7YicF5vPZQaHchwXcQ/Im3pFJG9Wa2iYqFmaOux5eY45U75urhnS2+hkvJRR1EsDuIZhnM2EUyhpPHcMd4Sa6Z//gC34ncv4FaAXXnvFzndAsFYZf5V91v9B5yEvQegKsG0EnM/4J1IpOdbNO2p53m7Y47woq6fdQZpsG7MjLdl9dzCqgoKuwCFztJDNfZKk4EAq220PkFJYHISRAwWIAi65JYHw9d+0Be5aMYVEL87rkC5XruVyiSjj54LeJLQ==',
      authnRequestBinding: null,
      skipRequestCompression: null,
    },
    {
      domain: 'hbc.com',
      entryPoint: 'https://hbctech.okta.com/app/s5a1_namogoo_1/exk28kybgrJRoxKmb2p7/sso/saml',
      issuer: 'http://www.okta.com/exk28kybgrJRoxKmb2p7',
      cert: 'MIIDpDCCAoygAwIBAgIGAV2meqXrMA0GCSqGSIb3DQEBCwUAMIGSMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCnM1YWhiY3RlY2gxHDAaBgkqhkiG9w0BCQEWDWluZm9Ab2t0YS5jb20wHhcNMTcwODAzMDUwMzAyWhcNMjcwODAzMDUwNDAxWjCBkjELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApzNWFoYmN0ZWNoMRwwGgYJKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqpOJt2biOU2UvhXZCUdPIHkJP/yuMDSNfP3lKAXqu+6UJt0SAjuUv7zAyllOPZQCXk2qSZKddNfKNzk5TlEHmeTcOfdiNqIVglILDnOmeMPrIazsU1s4PdWLvdD4k7OzAGHeHtJVqIvnCZxpeDqc4+f/TbfVUGB0L8NrRJRDhTNo6aD9G3WNq6JOH+psdaiEcoqO22/lFiMawGkN6/KkJkaeujt9MWAKYEQbmWV+BSnVuwWnaEDFws7dOtbn/veKi31ZT2PVQ4LYt9PwAUn7B26QJDG0ANrJzW5zBFsiaGiJqyyA1y9y83yrPcFoY7RSl2iFdzKy5dUyZKXlRRvrlQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQCQU5TGn2Xa/rY2xqWX5Xbu0GemJbKk82KltliK1zPjl7XIsP384WpMl27VHM+bb1h4KL6jA5QLtrZh7EWcx/JJpUv6O/MtC79+POJ/yoX/A+LhN/pjb9ypx6H16xvX8RtaqiWZjTaWWCBbS1TSF4+XswN6sw3hw57MCTrtMSEdjBFNgWx8kQe9bTa66IkIFo8Vz2YeWJO/zZ0Xf/Hk31tMEXF+dQN6td0l1N4Xcnwnt6d2sm017Ok9lQnSTaNqWZmnWbF+dpTErl+wNd8u6Ytao9V4nrqv0cW/TBhYsMtt9VBiI7TxAJEOXLyLxuTzF73nyYYaRXPiiQCfP8ktXC76',
      authnRequestBinding: null,
      skipRequestCompression: null,
    },
  ];
};

const buildSSOObj = (client) => {
  const clientObj = {
    domain: client.domain,
    ssoObj: {
      name: `${client.domain} SSO}`,
      callbackUrl: `https://chp.namogoo.com/auth/sso/${client.domain}/callback`,
      entryPoint: client.entryPoint,
      issuer: client.issuer,
      privateCert: fs.readFileSync(path.join(__dirname, '../../../../', 'namogoo.pem'), 'utf-8'),
      // client cert
      cert: client.cert,
      signatureAlgorithm: 'sha256',
      authenticateURL: `/auth/sso/${client.domain}`,
      skipRequestCompression: true,
      authenticateCallbackURL: `/auth/sso/${client.domain}/callback`,
      successRedirect: successLoginRedirect,
      failureRedirect: failureLoginRedirect,
    },
  };

  if (clientObj.authnRequestBinding) clientObj.ssoObj.authnRequestBinding = clientObj.authnRequestBinding;
  if (clientObj.skipRequestCompression) clientObj.ssoObj.skipRequestCompression = clientObj.skipRequestCompression;

  return clientObj;
};

const ssoCallback = async (profile, done) => {
  let user;
  try {
    debug(profile);
    logger.debug(`Gap.com SSO: ${profile.nameID || profile.email || profile.Email}`, { profile });
    user = { username: profile.nameID || profile.email || profile.Email };
    if (await authenticationCtrl.isAuthenticated({
      username: user.username,
      password: undefined,
      isApproved: false,
      isActive: false,
    })) {
      logger.debug(`${user && user.username}: Authorized`, { username: user && user.username });
      return done(null, user);
    }
    logger.debug(`${user && user.username}: Unauthorized`, { username: user && user.username });
    return done();
  } catch (err) {
    logger.error(`${user && user.username}: Unauthorized`, { username: user && user.username, error: err });
    return done();
  }
}

module.exports = {
  getSSOClientsConfiguration,
  buildSSOObj,
  ssoCallback,
}
