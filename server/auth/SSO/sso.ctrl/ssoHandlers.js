const login = require('nmg-utils').login; // the login handler
const { getSSOClientsConfiguration, buildSSOObj, ssoCallback } = require('./ssoFunctions');

module.exports = async (router) => {
  const ssoClients = await getSSOClientsConfiguration();
  const clientsHandlers = ssoClients.map(client => buildSSOObj(client));
  return clientsHandlers.map(clientObj => ({
    domain: clientObj.domain,
    handler: login.setupSSOAuthentication(router, `${clientObj.domain}SSO`, clientObj.ssoObj, ssoCallback),
  }));
};
