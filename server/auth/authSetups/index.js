const login = require('nmg-utils').login; // the login handler

const { localAuthenticationSetup } = require('./localSetup');
const { googleAuthenticationObj, googleAuthenticationSetup } = require('./googleSetup');

const setupLoginAuthentications = (app) => {
  // setup the local login authentication
  login.setupLocalAuthentication(app, localAuthenticationSetup);

  // setup google authentication
  login.setupGoogleAuthentication(app, googleAuthenticationObj, googleAuthenticationSetup);

  /** SSO setup occur in the SSO Directory */
};

module.exports = {
  setupLoginAuthentications,
};
