const logger = require('nmg-utils').logger({ consoleOn: true, logzioOn: true });

const {
  isAuthenticated,
  isNamogooUser,
  successLoginRedirect,
  failureLoginRedirect,
  getUserPermissions,
} = require('../auth.ctrl/authentication.ctrl');

const googleAuthenticationObj = {
  clientID: '414789579884-s2oqpndq3oiivi1vv0medn4dn4bilthc.apps.googleusercontent.com',
  clientSecret: 'gRPILx45M-r147jIPuhanUKV',
  callbackURL: '/auth/google/callback',
  authenticateURL: '/auth/google',
  successRedirect: successLoginRedirect,
  failureRedirect: failureLoginRedirect,
  scope: ['profile', 'email'],
};

const googleAuthenticationSetup = async (accessToken, refreshToken, profile, done) => {
  let user;
  try {
    user = { username: profile.emails && profile.emails.length > 0 && profile.emails[0].value };

    const namogooUser = isNamogooUser(user);

    if (await isAuthenticated({
      username: user.username,
      password: undefined,
      isApproved: namogooUser,
      isActive: namogooUser,
    }) || namogooUser) {
      const permissions = await getUserPermissions({ username: user.username });
      logger.debug(`${user && user.username}: Authorized`, { username: user && user.username });
      return done(null, { ...user, permissions });
    }
    logger.debug(`${user && user.username}: Unauthorized`, { username: user && user.username });
    return done(`${user && user.username}: Unauthorized`);
  } catch (err) {
    logger.error(`${user && user.username}: Unauthorized`, { username: user && user.username, error: err });
    return done(`${user && user.username}: Unauthorized`);
  }
};

module.exports = {
  googleAuthenticationObj,
  googleAuthenticationSetup,
};
