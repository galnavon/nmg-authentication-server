const logger = require('nmg-utils').logger({ consoleOn: true, logzioOn: true });

const { isAuthenticated, getUserPermissions } = require('../auth.ctrl/authentication.ctrl');

const localAuthenticationSetup = async (username, password, done) => {
  try {
    if (!(await isAuthenticated({ username, password }))) {
      logger.debug(`${username}: Unauthorized`, { username });
      return done(null, false, { message: 'Unauthorized' });
    }
    logger.debug(`${username}: Authorized`, { username });
    const permissions = await getUserPermissions({ username });
    return done(null, { username, permissions, isLocal: true });
  } catch (err) {
    console.log(err)
    logger.error(`${username}: Unauthorized`, { username, error: err });
    return done(null, false, { message: 'Unauthorized' });
  }
};

module.exports = {
  localAuthenticationSetup,
};
