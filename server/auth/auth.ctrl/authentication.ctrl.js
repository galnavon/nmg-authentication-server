const SHA256 = require('crypto-js/sha256');
const userCtrl = require('../user/user.ctrl/user.ctrl');

const namogooUserRegexp = /@namogoo\.com$/i;
const isNamogooUser = ({ username }) => username && namogooUserRegexp.test(username);
const isAuthenticated = async ({ username, password, isApproved, isActive } = {}, details = {}) => {
  const logObject = {
    ...details,
    message: `CHP APP @ RDS >> authentication check >> ${username}`,
    username,
  };
  const user = await userCtrl.getUser({ username }, logObject);
  // unknown user
  if (!user) {
    // add the user, but not approved it and not active
    await userCtrl.insertUser({
      username,
      password: SHA256(password).toString(),
      isApproved: isApproved || false,
      isActive: isActive || false,
    });
    return false;
  }

  return user.isApproved && user.isActive && (password ? SHA256(password).toString() === user.password : true);
};

const getUserPermissions = async ({ username }) => {
  const permissions = await userCtrl.getUserPermissions({ username });
  return permissions.reduce((acc, val) => {
    acc[val.role] = val.mode;
    return acc;
  }, {});
};

const successLoginRedirect = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'dev' ? '/' : '//localhost:8080/';
const failureLoginRedirect = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'dev' ? '/login' : '//localhost:8080/';

module.exports = {
  isAuthenticated,
  isNamogooUser,
  successLoginRedirect,
  failureLoginRedirect,
  getUserPermissions,
};
