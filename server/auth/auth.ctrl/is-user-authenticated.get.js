const { isAuthenticated } = require('./authentication.ctrl');

module.exports = async (req, res) => {
  const { username } = req.query;
  const result = await isAuthenticated({ username });
  res.send(result);
};
