const SHA256 = require('crypto-js/sha256');
const { updateUserPassword } = require('./user.ctrl');

const deletionQuery = 'DELETE FROM `chp_app_user_reset_password_tokens_tbl` WHERE `token` = ?token;';

module.exports = async (req, res) => {
  try {
    const { query } = req;
    const { username, token } = query;
    const password = SHA256(query.password).toString();

    await updateUserPassword({ username, password });
    await res.rds(deletionQuery, { token });

    res.send('Success :D').status(200);
  } catch (error) {
    res.send(error).status(500);
  }
};
