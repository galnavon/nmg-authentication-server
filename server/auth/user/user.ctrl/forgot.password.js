const { mailer, db } = require('nmg-utils');
const moment = require('moment');
const uuidv4 = require('uuid/v4');

const { getHTMLTemplateByUrl } = require('../../../templates-helpers/reset-password-form');
const { getUser } = require('./user.ctrl');

const insertionQuery = `
  INSERT INTO chp_app_user_reset_password_tokens_tbl 
  (\`id\`, token, created_at) VALUES (?id, ?token, ?createdAt) 
  ON DUPLICATE KEY UPDATE token=?token, created_at=?createdAt ;
`;

const mailFile = async ({ username, loginUrl }) => {
  try {
    const resp = await mailer.sendMail({
      from: 'donotreply@namogoo.com',
      to: username,
      subject: 'CHP Request to Reset Password',
      html: getHTMLTemplateByUrl(loginUrl),
    });
    return resp;
  } catch (error) {
    return error;
  }
};

const sendEmailAndUpdateUserToken = async (req, res, { id }) => {
  try {
    const token = uuidv4();
    const { query } = req;
    const { username } = query;
    const redirectTo = 'https://chp.namogoo.com/';
    const loginUrl = `${redirectTo}#/login/?token=${token}&username=${username}`;
    const createdAt = moment().format('YYYY-MM-DD HH:mm:ss');
    // 2. update table with user and token
    await db.rds.query(insertionQuery, { id, token, createdAt });
    // 3. send mail with the token to the user
    await mailFile({ username, loginUrl });
    res.end('Success :D');
  } catch (error) {
    res.send(error).status(500);
  }
};

module.exports = async (req, res) => {
  const { query } = req;
  const { username } = query;
  try {
    // 1. check if user exists
    const userResponse = await getUser({ username }); // found user
    if (userResponse === null) {
      res.json({
        ok: false,
        reason: `Couldn't find user ${username}`,
      });
      return;
    }
    await sendEmailAndUpdateUserToken(req, res, userResponse); // proceed to mail and token
  } catch (error) {
    res.send(error).status(500); // couldn't find user
  }
};
