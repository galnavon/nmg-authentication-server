const userCtrl = require('./user.ctrl');

module.exports = async ({ query: { username } }, res) => {
  const data = await userCtrl.getUser({ username });
  res.json({ data });
};
