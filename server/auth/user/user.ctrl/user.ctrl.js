const rds = require('nmg-utils').db.rds;

const getUser = async ({ username } = {}, details = {}) => {
  const data = await rds.query(`  select * 
                                  from chp_app_users_tbl
                                  where
                                  username = ?username
                                  limit 1;`, { username }, details);

  return data.length > 0 ?
    data
      .map(u => ({
        id: u.id,
        username: u.username,
        password: u.password,
        isApproved: u.is_approved,
        isActive: u.is_active,
      }))[0]
    : null;
};

const insertUser = async ({ username, password, isApproved, isActive } = {}, details = {}) => rds.query(
  `INSERT INTO chp_app_users_tbl 
          (\`username\`,  \`password\`, \`is_approved\`, \`is_active\`) 
  VALUES  ( ?username, ?password, ?isApproved, ?isActive);`,
  { username, password, isApproved, isActive },
  details);

const updateUserPassword = async ({ username, password } = {}, details = {}) => rds.query(
  'UPDATE chp_app_users_tbl SET `password` = ?password WHERE `username` = ?username;',
  { password, username },
  details);

const updateUserIsApproved = async ({ username, isApproved } = {}, details = {}) => rds.query(
  'UPDATE chp_app_users_tbl SET `is_approved` = ?isApproved WHERE `username` = ?username;',
  { isApproved, username },
  details);

const updateUserIsActive = async ({ username, isActive } = {}, details = {}) => rds.query(
  'UPDATE chp_app_users_tbl SET `is_active` = ?isActive WHERE `username` = ?username;',
  { isActive, username },
  details);

const deleteUser = async ({ username } = {}, details = {}) => rds.query(
  'DELETE FROM `chp_app_users_tbl` WHERE `username` = ?username;',
  { username },
  details);

const getUserPermissions = async ({ username }) => rds.query(
  `SELECT role, mode FROM mai_user_roles_tbl ur
   join chp_app_users_tbl us on us.id = ur.user_id
   where us.username = ?username`,
  { username },
);

module.exports = {
  getUser,
  insertUser,
  updateUserPassword,
  updateUserIsApproved,
  updateUserIsActive,
  deleteUser,
  getUserPermissions,
};
