const rdsQuery = 'select token, created_at as createdAt from chp_app_user_invites_tokens_tbl where token=?token and email=?username';

const DAY = 1000 * 60 * 60 * 24;
const TOKEN_EXPIRED_MS = DAY * 14;

module.exports = async (req, res) => {
  try {
    const { query } = req;
    const { token, username } = query;
    const response = await res.rds(rdsQuery, { token, username });
    if (response.length === 0) {
      res.json({ ok: false, reason: 'Token not found' });
      return;
    }
    const { createdAt } = response[0];
    if ((+new Date(createdAt) + TOKEN_EXPIRED_MS) - Date.now() > 0) {
      // token did not expire yet, all good
      res.json({ ok: true });
    } else {
      res.json({ ok: false, reason: 'Token is expired' });
    }
  } catch (error) {
    res.send(error).status(500);
  }
};
