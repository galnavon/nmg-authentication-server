const express = require('express');

const router = express.Router();

const getUser = require('./user.ctrl/user.get');
const forgotPass = require('./user.ctrl/forgot.password');
const resetForgotPass = require('./user.ctrl/reset.forgotten.password');

/**
 * Error Handler Wrapper
 * @param fn
 */
const errorHandlerWrapper = global.errorHandlerWrapper;

// handling various type of requests
router.get('/get-user', errorHandlerWrapper(getUser));
router.get('/forgot-pass', errorHandlerWrapper(forgotPass));
router.get('/reset-forgot-pass', errorHandlerWrapper(resetForgotPass));

module.exports = router; // exporting router so that it can be used in app.js
