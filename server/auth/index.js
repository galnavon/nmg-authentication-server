const express = require('express');
const login = require('nmg-utils').login; // the login handler

/**
 * Error Handler Wrapper
 * @param fn
 */
global.errorHandlerWrapper = fn => (...args) => fn(...args).catch(args[2]);

const isUserAuthenticated = require('./auth.ctrl/is-user-authenticated.get');
const forgotPassCtrl = require('./user/user.ctrl/forgot.password');
const validateResetPassTokenCtrl = require('./user/user.ctrl/validate.reset.pass.token');
const validateInvitationTokenCtrl = require('./user/user.ctrl/validate.invitation.token');
const resetForgottenPassCtrl = require('./user/user.ctrl/reset.forgotten.password');
const setPassFromInvitationCtrl = require('./user/user.ctrl/invitation.password.set');

// importing our handlers for the request
const sso = require('./SSO/index');


const router = express.Router();

router.use('/is_authenticated', isUserAuthenticated);
// sso login
router.use('/loginSSO', sso);

// google login
router.get('/google_login', login.getGoogleStrategy(router));

// login post
router.post('/login', login.getLocalStrategy(router), (req, res) => {
  res.set('Location', '//localhost:8080/');
  res.status(200).send('Authorized');
});

router.get('/forgot-pass', forgotPassCtrl);
router.get('/reset-forgot-pass', resetForgottenPassCtrl);
router.get('/set-pass-invitation', setPassFromInvitationCtrl);
router.get('/validate-reset-pass-token', validateResetPassTokenCtrl);
router.get('/validate-invitation-token', validateInvitationTokenCtrl);

// logout and clear session
router.post('/logout', (req, res) => {
  req.session = null;
  res.status(200).send('Logged out');
});
module.exports = router; // exporting router so that it can be used in app.js
