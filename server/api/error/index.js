const express = require('express');

const router = express.Router();

const postError = require('./error.ctrl/error.post');

const errorHandlerWrapper = global.errorHandlerWrapper;

// handling various type of requests
router.post('/post-error', errorHandlerWrapper(postError));

module.exports = router; // exporting router so that it can be used in app.js
