const express = require('express');

/**
 * Error Handler Wrapper
 * @param fn
 */
global.errorHandlerWrapper = fn => (...args) => fn(...args).catch(args[2]);

// importing our handlers for the request
const error = require('./error/index');
const auth = require('../auth/index');
const users = require('./users/index');

const router = express.Router();

router.use('/error', error);
router.use('/auth', auth);
router.use('/users', users);

module.exports = router; // exporting router so that it can be used in app.js
