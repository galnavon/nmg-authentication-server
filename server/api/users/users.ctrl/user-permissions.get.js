const { getUserPermissions } = require('../../../auth/auth.ctrl/authentication.ctrl');

module.exports = async (req, res) => {
  const { username, isNamogooUser } = req.user;
  const permissions = await getUserPermissions({ username });
  res.json({
    permissions,
    isNamogooUser,
  });
};
