const express = require('express');

const router = express.Router();

const getUserPerMissions = require('./users.ctrl/user-permissions.get');

const errorHandlerWrapper = global.errorHandlerWrapper;

// handling various type of requests

router.get('/get-user-permissions', errorHandlerWrapper(getUserPerMissions));
module.exports = router; // exporting router so that it can be used in app.js
