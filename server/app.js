const express = require('express'); // web application framework
const helmet = require('helmet'); // Help secure Express apps with various HTTP headers
const cookieSession = require('cookie-session'); // stores the session data on the client within a cookie
const bodyParser = require('body-parser'); // body parsing middleware
const logger = require('nmg-utils').logger({ consoleOn: true, logzioOn: true });
const login = require('nmg-utils').login; // the login handler
const createDebug = require('debug');

const debug = createDebug('nmg-authentication-server:app');

const auth = require('./auth');
const { setupLoginAuthentications } = require('./auth/authSetups');
const authentication = require('nmg-utils').authentication;

const app = express(); // setup the web server
try {
  app.use((req, res, next) => {
    res.namogooStart = Date.now();
    next();
  });

  // add the cookie-session object
  const maxAge = (60 * (60 * 1000)); // 1 hour
  app.use(cookieSession({
    name: 'session',
    keys: ['qbm4dk1qz1u6z', 'aw2lalpcxr256'],
    maxAge,
    domain: process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'dev' ? 'namogoo.com' : 'localhost',
    cookie: {
      secure: process.env.NODE_ENV === 'production',
    },
  }));

  // use helmet
  // app.use(helmet());

  // CORS for all
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:8080');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
  });

  // use bodyParser
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  // setup the authentication - after the session setup
  login.setupAuthentication(app);

  setupLoginAuthentications(app);

  // Update a value in the cookie so that the set-cookie will be sent.
  // Only changes every 5 minutes so that it's not sent with every request.
  app.use((req, res, next) => {
    const nowInMinutes = Math.floor(Date.now() / (5 * 60e3));
    req.session.nowInMinutes = req.session.nowInMinutes === nowInMinutes ? req.session.nowInMinutes : nowInMinutes;
    req.session.id = req.session.id ? req.session.id : [...Array(30)].map(() => Math.random().toString(36)[3]).join('');
    req.session.user = req.user;
    next();
  });

  app.use((req, res, next) => {
    logger.debug(`Dispatching ${req.url} \nQuery: ${JSON.stringify(req.query)}`);
    next();
  });

  app.use('/auth', auth);

  app.use('/api', authentication, require('./api'));


  const aliveSince = new Date();
  app.get('/health-check', (req, res) => res.status(200).json({
    aliveSince,
    status: 'ok!',
  }));

  // express has executed all middleware functions and routes, and found that none of them responded, so we will return 404 Not Found
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    logger.error(`${req.user && req.user.username}: ${req.url}`,
      { url: req.url, queryParams: req.query, username: req.user && req.user.username, error: err, status: 404 });
    next(err);
  });

  // error handler - error-handling middleware must be the last, after other app.use() and routes calls
  // has four args: err, req, res, next
  app.use((err, req, res) => {
    logger.error(`${req.user && req.user.username}: ${req.url}`,
      { url: req.url, queryParams: req.query, username: req.user && req.user.username, error: err, status: 500 });
    res.status(500).send(err);
  });
} catch (e) {
  console.log(e)
  logger.error(e);
}

module.exports = app;
