#!/bin/bash
#tagger

VER=${1:-patch}

git checkout master
git reset --hard origin/master
npm version $VER
git push --tags
git push origin master

git checkout develop
git reset --hard origin/develop
git pull origin master
git push origin develop
