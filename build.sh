#!/usr/bin/env bash
VERSION='1.0.0'
IMAGE_NAME='045333963526.dkr.ecr.us-east-1.amazonaws.com/nmg-authentication-server'
cd /namogoo/data/docker/nmg-authentication-server
git pull origin master
if [ $? -ne 0 ]; then
    echo "Failed to pull master branch from backend repository"
    exit 1
fi
docker build --no-cache -t ${IMAGE_NAME}:${VERSION} --build-arg ssh_prv_key="$(cat /root/.ssh/id_rsa)" --squash .
if [ $? -ne 0 ]; then
    echo "Failed to build docker image"
    exit 1
fi
$(/usr/local/bin/aws ecr get-login --no-include-email --region us-east-1)
docker push ${IMAGE_NAME}:${VERSION}
